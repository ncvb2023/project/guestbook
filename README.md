## Mã nguồn dữ án Guestbook

## Yêu cầu công cụ
1. **Docker 19.03** or above and permission to [manage Docker as a non-root user](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)
2. Python3 và Pip3 [Bài hướng dẫn cài đặt](https://vinasupport.com/huong-dan-cai-dat-python-3-va-pip-3-tren-ubuntu-linux/)


## Khởi chạy
### Khởi chạy dịch vụ Redis
- Vì dự án Guestbook yêu cầu giải pháp caching, trong dự án này yêu cầu dịch vụ Redis.
- Để khởi chạy dịch vụ Redis chúng ta thực hiện các câu lệnh như sau:
```sh
# khoi chay dich vu redis bang docker
$ docker run --name redis -d -p 6379:6379 redis:6.2.10-alpine

# kiem tra dich vu redis da chay duoc chua?
$ docker ps
CONTAINER ID   IMAGE                 COMMAND                  CREATED         STATUS         PORTS                                       NAMES
c7ed0d052fc4   redis:6.2.10-alpine   "docker-entrypoint.s…"   2 seconds ago   Up 2 seconds   0.0.0.0:6379->6379/tcp, :::6379->6379/tcp   redis
```

### Khởi chạy dịch vụ guestbook trên máy tính
```
$ export REDIS_URL=localhost
$ cd src/
$ pip3 install -r requirements.txt
$ python3 app.py
```

Truy cập dịch vụ Guestbook bằng trình duyệt tại địa chỉ localhost:8080
![Guestboook application](docs/guestbook_screenshot.png)



## Tham khảo
- https://github.com/GoogleCloudPlatform/container-vm-guestbook-redis-python
